import { todoService } from "./dataServices.js";
import {
  todoInput,
  todoRender,
  filterArr,
  kiemTraChu,
  spinner,
} from "./controller.js";

let trueValue = true;
let falseValue = false;
// Lấy data từ server để render giao diện
let renderTodoService = () => {
  todoService
    .axiosGet()
    .then((res) => {
      let dataArr = res.data;

      // Mảng chứa task đã hoàn thành, lọc dữ liệu từ key "Boolean:true"
      let completedArr = filterArr(dataArr, trueValue);
      // Mảng chứa task chưa hoàn thành, lọc dữ liệu từ key "Boolean:false"
      let uncompletedArr = filterArr(dataArr, falseValue);

      todoRender(uncompletedArr, "#todo");
      todoRender(completedArr, "#completed");
    })
    .catch((err) => {});
};
renderTodoService();

// Hàm cập nhật dữ liệu
let dataUpdate = (task) => {
  todoService
    .axiosPut(task)
    .then((res) => {})
    .catch((err) => {});
};

// Hàm thêm dữ liệu vào server
let postTask = () => {
  spinner.onLoading();
  let valueInput = document.querySelector("#newTask").value;
  let isValid = true;

  isValid = kiemTraChu(valueInput, "#warning", "Task");

  if (!isValid) {
    spinner.offLoading();
    return;
  }
  todoService
    .axiosPost(todoInput())
    .then((res) => {
      renderTodoService();
      spinner.offLoading();
    })
    .catch((err) => {
      spinner.offLoading();
    });
};
window.postTask = postTask;

// Xóa dữ liệu
let deleteTask = (id) => {
  spinner.onLoading();
  todoService
    .axiosDelete(id)
    .then((res) => {
      renderTodoService();
      spinner.offLoading();
    })
    .catch((err) => {
      spinner.offLoading();
    });
};
window.deleteTask = deleteTask;

// Hàm check những task đã hoàn thành, mỗi khi check thì đảo giá trị boolean và cập nhật lên server
let completed = (taskId) => {
  todoService
    .axiosGet()
    .then((res) => {
      let dataArr = res.data;
      let index = dataArr.findIndex((item) => {
        return item.id == taskId;
      });

      let changeBoolean = !dataArr[index].Boolean;
      let newtask = { ...dataArr[index], Boolean: changeBoolean };

      dataUpdate(newtask);
      renderTodoService();

      setTimeout(renderTodoService(), 0);
    })
    .catch((err) => {});
  setTimeout(renderTodoService(), 0);
};
window.completed = completed;
setTimeout(renderTodoService(), 0);

// sort a-z
document.querySelector("#two").addEventListener("click", () => {
  todoService
    .axiosGet()
    .then((res) => {
      let dataArr = res.data;
      let sortData = dataArr.sort((a, b) => (a.name > b.name ? 1 : -1));

      let completedArr = filterArr(sortData, trueValue);
      let uncompletedArr = filterArr(sortData, falseValue);

      todoRender(uncompletedArr, "#todo");
      todoRender(completedArr, "#completed");
    })
    .catch((err) => {
    });
});
// sort z-a
document.querySelector("#three").addEventListener("click", () => {
  todoService
    .axiosGet()
    .then((res) => {
      let dataArr = res.data;
      let sortData = dataArr.sort((a, b) => (a.name < b.name ? 1 : -1));

      let completedArr = filterArr(sortData, trueValue);
      let uncompletedArr = filterArr(sortData, falseValue);

      todoRender(uncompletedArr, "#todo");
      todoRender(completedArr, "#completed");
    })
    .catch((err) => {});
});
