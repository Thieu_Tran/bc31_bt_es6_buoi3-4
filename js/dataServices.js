let url_Base = "https://62b9758941bf319d227cec4b.mockapi.io/";

export let todoService = {
  axiosPost: (task) => {
    return axios({
      url: url_Base + "todoList",
      method: "POST",
      data: task,
    });
  },
  axiosGet: () => {
    return axios({
      url: url_Base + "todoList",
      method: "GET",
    });
  },
  axiosDelete: (id) => {
    return axios({
      url: url_Base + "todoList/" + id,
      method: "DELETE",
    });
  },

  axiosPut: (task) => {
    return axios({
      url: url_Base + "todoList/" + task.id,
      method: "PUT",
      data: task,
    });
  },
};
