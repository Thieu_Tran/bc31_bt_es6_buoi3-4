export let todoInput = () => {
  let valueInput = document.querySelector("#newTask").value;

  let newTask = {
    name: valueInput,
  };
  document.querySelector("#newTask").value = "";
  return newTask;
};

export let todoRender = (list, Selector) => {
  let changeClass = (value) => {
    if (value == true) {
      return "fas";
    } else {
      return "far";
    }
  };

  let renderHTML = "";
  for (let index = 0; index < list.length; index++) {
    let task = list[index];
    let contentUl = `<li>
                            <span>${task.name}</span>
                            <div class="buttons">
                                <button class="remove" onclick="deleteTask(${
                                  task.id
                                })">
                                    <i class="fa fa-trash-alt"></i>
                                </button>
                                <button class="complete" onclick="completed('${
                                  task.id
                                }')">
                                    <i class= 'fa fa-check-circle ${changeClass(
                                      task.Boolean
                                    )}'></i>
                                </button>
                            </div>
                        </li>
                        `;
    renderHTML += contentUl;
  }
  document.querySelector(Selector).innerHTML = renderHTML;
};

export let filterArr = (data, isValue) => {
  return data.filter((item) => {
    return item.Boolean == isValue;
  });
};

export let  kiemTraChu = (value, selectorError, name) =>{
    var regexLetter = /^[A-Z a-z]+$/;
    if (value.length == 0) {
      document.querySelector(selectorError).innerHTML =
        name + " không được bỏ trống.";
      return false;
    } else if (regexLetter.test(value)) {
      document.querySelector(selectorError).innerHTML = "";
      return true;
    } else {
      document.querySelector(selectorError).innerHTML =
        name + ", tất cả phải là chữ cái không dấu.";
      return false;
    }
  }

  export const spinner ={
    onLoading:()=>{
        document.getElementById("loading").style.display ="flex";
        document.getElementById("tableTask").style.display ="none";
    },
    offLoading:()=>{
        document.getElementById("loading").style.display ="none";
        document.getElementById("tableTask").style.display ="block";
    },
}

  
